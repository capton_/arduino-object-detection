FROM nvidia/cuda:11.2.0-cudnn8-devel-ubuntu20.04

# Install dependencies
RUN apt-get update
RUN DEBIAN_FRONTEND="noninteractive" TZ="Europe/Paris" apt-get -y install git wget tzdata unzip \
                                                        libopencv-dev cmake python3 python3-pip

RUN mkdir -p /workdir
WORKDIR /workdir

# Download weights
RUN mkdir weights && cd weights && wget https://github.com/AlexeyAB/darknet/releases/download/darknet_yolo_v4_pre/yolov4-tiny.conv.29

# Build darknet
RUN git clone https://github.com/AlexeyAB/darknet
RUN sed -i '/cmake_minimum_required(VERSION 3.18)/c\cmake_minimum_required(VERSION 3.16)' /workdir/darknet/CMakeLists.txt
RUN ln -s /usr/local/cuda/lib64/stubs/libcuda.so /usr/local/cuda/lib64/stubs/libcuda.so.1
RUN cd darknet && mkdir build_release && cd build_release && cmake .. && \
    LD_LIBRARY_PATH=/usr/local/cuda/lib64/stubs/:$LD_LIBRARY_PATH cmake --build . --target install --parallel 8
RUN rm /usr/local/cuda/lib64/stubs/libcuda.so.1

ADD requirements.txt /workdir/
# Install python dependencies
RUN pip install -r requirements.txt
RUN git clone https://github.com/hunglc007/tensorflow-yolov4-tflite
RUN sed -i 's/classes\/coco.names/obj.names/g' /workdir/tensorflow-yolov4-tflite/core/config.py

# Download dataset

ADD data /workdir/data
ADD scripts /workdir/scripts

RUN wget files.heuritech.com/raw_files/dataset_surfrider_cleaned.zip
RUN unzip dataset_surfrider_cleaned.zip && rm dataset_surfrider_cleaned.zip
RUN cd dataset_surfrider_cleaned/Images_md5 && find . -type f -exec mv '{}' '{}'.jpg \;
RUN mv dataset_surfrider_cleaned/Images_md5 data/obj
RUN mv dataset_surfrider_cleaned/dataset.json data/dataset_lines.json
RUN python3 scripts/convert_labels.py

ADD cfg /workdir/cfg

# Create user
RUN export uid=1000 gid=1000 && \
    mkdir -p /home/developer && \
    echo "developer:x:${uid}:${gid}:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
    echo "developer:x:${uid}:" >> /etc/group && \
    chown ${uid}:${gid} -R /home/developer && \
    chown ${uid}:${gid} -R /workdir

USER developer

CMD bash