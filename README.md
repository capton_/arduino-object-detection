# arduino-object-detection

# Docker

## BUILD

```sh
$ docker build -t local/yolo-tflite .
```

## RUN

```sh
$ docker run --gpus all -it --rm -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v $PWD/output:/workdir/output local/yolo-tflite
```

## Fine-tuning Yolo v4 Tiny

```sh
$ ./darknet/darknet detector train data/obj.data cfg/yolo-tiny-obj.cfg weights/yolov4-tiny.conv.29 -map
```

## Convert to Tensorflow

* Run command

```sh
$ python3 tensorflow-yolov4-tflite/save_model.py --weights ./output/darknet_models/yolo-tiny-obj_best.weights --output ./output/tf-models/yolov4-tiny-416 --input_size 416 --model yolov4 --tiny --framework tflite
```

# Quantization

```sh
$ python3 scripts/tflite_converter.py ./output/tf-models/yolov4-tiny-416 --output_dir ./output/quant_models --quant [float16 or int8]
```

# Inference

* Run benchmark on a given number of images
```sh
$ python3 scripts/detect.py --benchmark [nbimage] [path_to_weights]
```

* Run prediction on a single image
```sh
$ python3 scripts/detect.py --img [path_to_image] [path_to_weights]
```
NB: "preds" directory must be created in "data" before running prediction on single image