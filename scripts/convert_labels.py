import json

classes_file = open('./data/classes.json')
classes = json.load(classes_file)
classes_file.close()

classes_ids = { classes[i]:i for i in range(0, len(classes) ) }


# box form[x,y,w,h]
def convert(size, box):
    dw = 1. / size[0]
    dh = 1. / size[1]
    x = (box[0] + box[2]) * dw / 2
    y = (box[1] + box[3]) * dh / 2
    w = (box[2] - box[0]) * dw
    h = (box[3] - box[1]) * dh
    return (x, y, w, h)


def convert_annotation():
    with open('./data/dataset_lines.json', 'r') as f:
        for line in f:
            data = json.loads(line)
            width = 600
            height = 600
            md5 = data["md5"]
            labels = data["labels"]
            outfile = open(f'./data/obj/{md5}.txt', 'a+')
            for item in labels:
                cls = item["label"]
                box = list(map(int, item["bbox"]))
                bb = convert((width, height), box)
                outfile.write(str(classes_ids[cls]) + " " + " ".join([str(a) for a in bb]) + '\n')
        outfile.close()

if __name__ == '__main__':
    convert_annotation()
