import tensorflow as tf
import cv2
import numpy as np
from tensorflow.python.ops.math_ops import argmax
from utils import draw_bbox
from PIL import Image
import os
import argparse
from convert_labels import convert as normalize_shape

OUTPUT_DIR = "output/preds"

def read_img(img: str, input_size: int) -> np.array:
    original_image = cv2.imread(img)
    original_image = cv2.cvtColor(original_image, cv2.COLOR_BGR2RGB)

    image_data = cv2.resize(original_image, (input_size, input_size))
    image_data = image_data / 255.

    return np.asarray([image_data]).astype(np.float32), original_image


def filter_boxes(box_xywh, scores, score_threshold=0.4, input_shape = tf.constant([416,416])):
    scores_max = tf.math.reduce_max(scores, axis=-1)

    mask = scores_max >= score_threshold
    class_boxes = tf.boolean_mask(box_xywh, mask)
    pred_conf = tf.boolean_mask(scores, mask)
    class_boxes = tf.reshape(class_boxes, [tf.shape(scores)[0], -1, tf.shape(class_boxes)[-1]])
    pred_conf = tf.reshape(pred_conf, [tf.shape(scores)[0], -1, tf.shape(pred_conf)[-1]])

    box_xy, box_wh = tf.split(class_boxes, (2, 2), axis=-1)

    input_shape = tf.cast(input_shape, dtype=tf.float32)

    box_yx = box_xy[..., ::-1]
    box_hw = box_wh[..., ::-1]

    box_mins = (box_yx - (box_hw / 2.)) / input_shape
    box_maxes = (box_yx + (box_hw / 2.)) / input_shape
    boxes = tf.concat([
        box_mins[..., 0:1],  # y_min
        box_mins[..., 1:2],  # x_min
        box_maxes[..., 0:1],  # y_max
        box_maxes[..., 1:2]  # x_max
    ], axis=-1)

    return (boxes, pred_conf)


def detect(img_path: str, weigths: str, input_size: int) -> None:
    preprocessed_img, original_img = read_img(img_path, input_size)

    model = tf.lite.Interpreter(model_path=weigths)
    model.allocate_tensors()
    input_details = model.get_input_details()
    output_details = model.get_output_details()
    model.set_tensor(input_details[0]['index'], preprocessed_img)
    model.invoke()
    pred = [model.get_tensor(output_details[i]['index']) for i in range(len(output_details))]
    
    boxes, pred_conf = filter_boxes(pred[0], pred[1], score_threshold=0.25, input_shape=tf.constant([input_size, input_size]))
    boxes, scores, classes, valid_detections = tf.image.combined_non_max_suppression(
        boxes=tf.reshape(boxes, (tf.shape(boxes)[0], -1, 1, 4)),
        scores=tf.reshape(
            pred_conf, (tf.shape(pred_conf)[0], -1, tf.shape(pred_conf)[-1])),
        max_output_size_per_class=50,
        max_total_size=50,
        iou_threshold=0.45,
        score_threshold=0.25
    )


    pred_bbox = [boxes.numpy(), scores.numpy(), classes.numpy(), valid_detections.numpy()]

    image = draw_bbox(original_img, pred_bbox)
    image = Image.fromarray(image.astype(np.uint8))
    image = cv2.cvtColor(np.array(image), cv2.COLOR_BGR2RGB)

    base_path = img_path.split('/')
    if not os.path.exists(OUTPUT_DIR):
        os.makedirs(OUTPUT_DIR)
    cv2.imwrite(f'{OUTPUT_DIR}/{base_path[-1]}', image)

    return pred_bbox


def bb_intersection_over_union(boxA, boxB):
	# determine the (x, y)-coordinates of the intersection rectangle
	xA = max(boxA[0], boxB[0])
	yA = max(boxA[1], boxB[1])
	xB = min(boxA[2], boxB[2])
	yB = min(boxA[3], boxB[3])
	# compute the area of intersection rectangle
	interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)
	# compute the area of both the prediction and ground-truth
	# rectangles
	boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
	boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)
	# compute the intersection over union by taking the intersection
	# area and dividing it by the sum of prediction + ground-truth
	# areas - the interesection area
	iou = interArea / float(boxAArea + boxBArea - interArea)
	# return the intersection over union value
	return iou

def benchmark(subset=10, directory='data/obj', weights='./quant_models/yolov4-tiny-quant.tflite') -> None:
    """
    Benchmarks with `subset` samples.
    """
    tp_pred = np.zeros(3)
    tp_true = np.zeros(3)

    cpt = 0
    avg_io = []
    for root, _, files in os.walk(f'./{directory}/'):
        for i in files:
            if cpt == subset - 1:
                break
            f = os.path.join(f'{root}', i)
            predictions = detect(img_path=f, weigths=weights, input_size=416)
            nb_elm_detectd = predictions[3][0]
            pred_boxes, pred_score, pred_classes = predictions[0][:nb_elm_detectd], predictions[1][0][:nb_elm_detectd], predictions[2][0][:nb_elm_detectd]
            filename = i.split('.')
            gt = f'{directory}/{filename[0]}.txt'
            
            with open(gt) as ground_truth:
                y_trues = ground_truth.readlines()

                iou_sum = 0
                precision = 0

                already_matched = []
                for pred_i in range(nb_elm_detectd):
                    
                    tp_true[int(pred_classes[pred_i])] += 1

                    if pred_score[pred_i] < 0.25:
                            continue
                            
                    max_arr = []
                    for gt_index in range(len(y_trues)):

                        if gt_index in already_matched:
                            continue

                        true_boxe = [float(i) for i in y_trues[gt_index].split(' ')[1:]]
                        normalized_pred_boxes = normalize_shape((600, 600), pred_boxes[0][pred_i])
                        max_arr.append(bb_intersection_over_union(true_boxe, list(normalized_pred_boxes)))
                    
                    if len(max_arr) > 0 and int(y_trues[argmax(max_arr)].split(' ', 1)[0]) == int(pred_classes[pred_i]):
                        iou_sum += max(max_arr)
                        precision += 1
                        tp_pred[int(pred_classes[pred_i])] += pred_score[pred_i]
                        already_matched.append(argmax(max_arr))
                    
                iou_sum /= len(y_trues)
                avg_io.append(iou_sum)

            cpt += 1

    return np.mean(avg_io), np.mean(tp_pred/tp_true)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Run prediction on TFLite yolo model')
    parser.add_argument('--img', type=str, help='image to detect object from')
    parser.add_argument('--benchmark', type = int, help='launch benchmark on provided number of images')
    parser.add_argument('weights', type=str, help='Yolov4-tiny tflite weights')

    args = parser.parse_args()
    if (args.benchmark):
        avg_iou, precision = benchmark(subset=args.benchmark, weights = args.weights)
        print(f"For conf_thresh = 0.25, precision: {precision} average IoU: {avg_iou}")
    else:
        detect(img_path=args.img, weigths=args.weights, input_size=416)