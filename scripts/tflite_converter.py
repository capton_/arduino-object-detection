import tensorflow as tf
import argparse
import os

def convert_yolo_tflite(tf_yolo_path, output_path, quant_method):
    """Apply quantization on a tensorflow model
    """
    converter = tf.lite.TFLiteConverter.from_saved_model(tf_yolo_path)

    if quant_method == "float16":
        converter.target_spec.supported_types = [tf.float16]
    else:
        converter.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS_INT8]
    
    converter.optimizations = [tf.lite.Optimize.DEFAULT]
    converter.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS, tf.lite.OpsSet.SELECT_TF_OPS]
    converter.allow_custom_ops = True

    tflite_model = converter.convert()

    if not os.path.exists(output_path):
        os.makedirs(output_path)

    output_file = f'{output_path}/yolov4-tiny-{quant_method}.tflite'
    with open(output_file, 'wb') as f:
        f.write(tflite_model)

    print("Model saved at", output_file)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Convert a Yolo model to TFLite using quantization.')
    parser.add_argument('model_path', type=str, help='The path to the Yolo model')
    parser.add_argument('--output_dir', type=str, default="./output/quant_models",
                        help='The directory where to store the model')
    parser.add_argument('--quant', type=str, default="float16", help='float16 or int8')

    args = parser.parse_args()
    convert_yolo_tflite(args.model_path, args.output_dir, args.quant)